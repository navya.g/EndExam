package just.testNG;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class MyTestNg {
	public void myAS() {
		System.out.println("Inside  My After Suite...");
		
	}
	
	@BeforeSuite
	public void myBS(){
		System.out.println("Inside My Befor Suite");
	}
	
	@BeforeClass
	public void myBC() {
		System.out.println("Inside Before Class...");

	}
	
	@AfterClass
	public void myAC() {
		System.out.println("Inside After method...");
		
	}
	
	@AfterTest
	public void myAT() {
		System.out.println("Inside After Test...");
		
	}
	
	
	@AfterMethod
	public void tearDown() {
		System.out.println("Inside After method...");
		
	}

	@BeforeMethod
	public void tearDown1() {
		System.out.println("Inside Before method...");

	}


	@Test (priority =1)
	public void myTest1() {
		System.out.println("Inside test 1...");

	}

	@Test(priority =2)
	public void myTest2() {
		System.out.println("Inside test 2...");
		

	}

}

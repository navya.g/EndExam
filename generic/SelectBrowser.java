/*
 * Date: September 1st 2014
 * Architect: Yagnesh Shah
 * Contributor: Yagnesh Shah
 * Twitter handle: @YagneshHShah
 * Contact: yash.shah.g@gmail.com / yagnesh23.wordpress.com 
 * License Type: MIT
 */

package BTAC.SASample.utils.generic;

import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
//import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;

import rest.impl.IronWasp;
import btac.sa.lib.FilesAndFolders;
import fr.jayasoft.ivy.License;

// TODO: Auto-generated Javadoc
/**
 * The Class SelectBrowser.
 */
public class SelectBrowser {

	/**
	 * Instantiates a new select browser.
	 */
	public SelectBrowser()
	{
		super();
	}

	/**
	 * Gets the browser.
	 *
	 * @return the browser
	 * @throws Exception 
	 */
	public static WebDriver getBrowser() throws Exception 
	{
		WebDriver driver = null;
		//HtmlUnitDriver htmlDriver = null;
		
		String browser = FilesAndFolders.getPropValue("driverName");
		System.out.println("Browser name is :" + browser);
		

		//Select Browser logic
		//HtmlUnitDriver
//		if(browser.equalsIgnoreCase("htmlunitdriver"))
//		{
//			htmlDriver = new HtmlUnitDriver();//use this if u dont wish JS to be enabled
//			//htmlDriver = new HtmlUnitDriver(true);//use this if u wish JS to be enabled. By default it will emulate IE's Javascript 
//			//htmlDriver = new HtmlUnitDriver(BrowserVersion.FIREFOX_24);//use this if u wish JS to be enabled. By default it will emulate FF24's Javascript
//			System.out.println("Browser session initiated...");
//		}

		//Firefox
		if(browser.equalsIgnoreCase("firefox")){
			driver = new FirefoxDriver();
			System.out.println("Browser session initiated...");
		}

		else if(browser.equalsIgnoreCase("firefoxFirebug")){		
			String firebugAddon = FilesAndFolders.getPropValue("firebugAddon");
			String firepathAddon = FilesAndFolders.getPropValue("firepathAddon");
			
			FirefoxProfile profile = new FirefoxProfile();
			File firebug = new File(firebugAddon);
			File firepath = new File(firepathAddon);
			profile.addExtension(firebug);
			profile.setPreference("extensions.firebug.currentVersion", "2.0");
			profile.addExtension(firepath);
			driver = new FirefoxDriver(profile);
			System.out.println("Browser session initiated...");
		}
		
		else if(browser.equalsIgnoreCase("firefoxIronWasp"))
		{
			FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("network.proxy.type", 1);
			profile.setPreference("network.proxy.http", IronWasp.ipAddress);
			profile.setPreference("network.proxy.http_port", IronWasp.portNumber);
			profile.setPreference("network.proxy.ssl", IronWasp.ipAddress);
			profile.setPreference("network.proxy.ssl_port", IronWasp.portNumber);
			profile.setPreference("network.proxy.no_proxy_on","");
			driver = new FirefoxDriver(profile);
			System.out.println("Browser session initiated...");
		}

		else if(browser.equalsIgnoreCase("firefoxWithZap")){	
			String PROXY = "localhost:8080";

			org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();
			proxy.setHttpProxy(PROXY)
			     .setFtpProxy(PROXY)
			     .setSslProxy(PROXY);
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability(CapabilityType.PROXY, proxy);
			
			driver = new FirefoxDriver(cap);
			System.out.println("Browser session initiated...");
		}
		else if(browser.equalsIgnoreCase("firefox_WP")){
			FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("browser.link.open_newwindow", 3);
			driver = new FirefoxDriver(profile);
			System.out.println("Browser session initiated...");
		}
		else if(browser.equalsIgnoreCase("firefoxUbuntu64"))
		{
			String Xport = System.getProperty("lmportal.xvfb.id",":22");
			final File firefoxPath = new File(System.getProperty("lmportal.deploy.firefox.path","/usr/bin/firefox"));
			FirefoxBinary firefoxBinary = new FirefoxBinary(firefoxPath);
			firefoxBinary.setEnvironmentProperty("DISPLAY",Xport);

			driver = new FirefoxDriver();
//			driver = new FirefoxDriver(firefoxBinary,null);
			System.out.println("Browser session initiated...");
		}
		
		//IE
		else if(browser.equalsIgnoreCase("ieWinx32"))
		{
			String ieWinx32 = FilesAndFolders.getPropValue("ieWinx32"); 
			System.setProperty("webdriver.ie.driver", ieWinx32); // setting path of the IEDriver
			DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
			ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			driver = new InternetExplorerDriver(ieCapabilities);
			System.out.println("Browser session initiated...");
		}
		else if(browser.equalsIgnoreCase("ieWinx64"))
		{
			String ieWinx64 = FilesAndFolders.getPropValue("ieWinx64");
			System.setProperty("webdriver.ie.driver", ieWinx64); // setting path of the IEDriver
//			DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
//			ieCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			driver = new InternetExplorerDriver();
			System.out.println("Browser session initiated...");
		}
			
		//Safari
		else if(browser.equalsIgnoreCase("safari")){
			driver = new SafariDriver();
			System.out.println("Browser session initiated...");
		}
		
		//Chrome
		else if(browser.equalsIgnoreCase("chromeWinx32")){
			String chromeWinx32 = FilesAndFolders.getPropValue("chromeWinx32");
			System.setProperty("webdriver.chrome.driver", chromeWinx32); // setting path of the ChromeDriver
			driver = new ChromeDriver();
			System.out.println("Browser session initiated...");
		}
		else if(browser.equalsIgnoreCase("chromeMac32")){
			String chromeMac32 = FilesAndFolders.getPropValue("chromeMac32");
			System.setProperty("webdriver.chrome.driver", chromeMac32); // setting path of the ChromeDriver
			driver = new ChromeDriver();
			System.out.println("Browser session initiated...");
		}
		else if(browser.equalsIgnoreCase("chromeLinux64")){
			String chromeLinux64 = FilesAndFolders.getPropValue("chromeLinux64");
			System.setProperty("webdriver.chrome.driver", chromeLinux64); // setting path of the ChromeDriver
			driver = new ChromeDriver();
			System.out.println("Browser session initiated...");
		}
		return driver;
	}
}

package practise.makemytrip.test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import btac.sa.lib.BrowserActions;
import btac.sa.lib.MouseAndKeyboard;
import btac.sa.lib.WebCommonMethods;
import practise.makemytrip.webmethods.searchmethods;
import util.generic.SelectBrowser;


public class searchTrip extends SelectBrowser {

	WebCommonMethods general;
	searchmethods searchMethod;
	BrowserActions ba;
	MouseAndKeyboard mk;

	String appserver;
	
	@BeforeMethod
    public void openTheBrowser() throws Exception 
    {
    	WebDriver d = getBrowser();
	    searchMethod = PageFactory.initElements(d, searchmethods.class);
	    general = PageFactory.initElements(d, WebCommonMethods.class);// initiating the driver and the .class file (the pageObject script)	    
	    ba = PageFactory.initElements(d, BrowserActions.class);// initiating the driver and the .class file (the pageObject script)	    
	    mk = PageFactory.initElements(d, MouseAndKeyboard.class);// initiating the driver and the .class file (the pageObject script)	    

	    BrowserActions.openURLBasedOnDbDomain();
    } 

	//Search trip flights test 
	@Test(priority=1, groups={"searchMethod"})
	public void validLoginCheck() throws Exception
	{	
		System.out.println("************************Searching started***************************");
		searchmethods.search("Bangalore, IN - Kempegowda International Airport (BLR)");
		//SearchMethods.logout();
		System.out.println("++++++++++++++++++++++++Searching success+++++++++++++++++++++++++++");
	}

	
	@AfterMethod(alwaysRun=true)
    public void catchExceptions(ITestResult result) throws Exception 
    {    
    	String methodname = result.getName();
        if(!result.isSuccess()){            
        	WebCommonMethods.screenshot(methodname);
        }
        BrowserActions.quit(); // Calling function close to quit browser instance
    }
}

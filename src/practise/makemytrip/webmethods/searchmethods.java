package practise.makemytrip.webmethods;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import btac.sa.lib.FilesAndFolders;
import btac.sa.lib.SelectBrowser;
import btac.sa.lib.WebCommonMethods;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;


public class searchmethods extends SelectBrowser {
	public static WebDriver driver;
	public searchmethods(WebDriver driver){
		this.driver=driver;
	}

	static Workbook wrk1;
	static Sheet sheet1;
	static Cell colRow;
	@FindBy(xpath = ".//*[html/body/div[2]/div[3]/div[1]/section/div[1]/div/div[2]/label]")
	public static WebElement radio;
	@FindBy(xpath = ".//*[@id='FromTag']")
	public static WebElement fromField;
	@FindBy(xpath=".//*[@id='ToTag']")
	public static WebElement toField;
	@FindBy(xpath=".//*[@id='DepartDate']")
	public static WebElement departField;
	@FindBy(xpath=".//*[@id='ReturnDate']")
	public static WebElement returnField;
	//
	@FindBy(xpath=".//*[@id='Adults']")
	public static WebElement adultsField;
	@FindBy(xpath=".//*[@id='SearchBtn']")
	public static WebElement btnField;
	
	@FindBy(xpath=".//*[@id='flightForm']/section[2]/div[3]/div[1]/h2")
	public static WebElement priceField;
	//.//*[@id='GlobalNav']/div/div[2]/div/strong
	@FindBy(xpath=".//*[@id='GlobalNav']/div/div[2]/div/strong")
	public static WebElement resField;

	public static void search(String userName) throws Exception
	{	
		radio.click();
		Thread.sleep(3000);
		WebCommonMethods.callingImplicitSleep();		
		Cell[] record = FilesAndFolders.readExcel("clearTrip",userName);  //sending userName, password

		System.out.println("record: "+ record.length);
		String from = record[1].getContents();
		String to = record[2].getContents();
		String depart = record[3].getContents();
		String ret = record[4].getContents();
		
		fromField.clear();
		fromField.sendKeys(from);
		Thread.sleep(2000);
		
		toField.clear();
		toField.sendKeys(to);
		Thread.sleep(2000);
		
		departField.click();
		departField.clear();
		departField.sendKeys(depart);
		Thread.sleep(3000);
		
		returnField.click();
		returnField.clear();
		returnField.sendKeys(ret);
		Thread.sleep(3000);
		
		adultsField.click();
		adultsField.sendKeys("2");
		Thread.sleep(3000);
		
		btnField.click();
		Thread.sleep(6000);
		
		String str=resField.getText();
		
		//validLogin(username,password,profileName);
		Assert.assertEquals(str, record[6].getContents());
		System.out.println("Search successfull..."+str);
	}



}
